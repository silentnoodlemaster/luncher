//
//  Lunch.swift
//  Luncher
//
//  Created by ben lönnqvist on 10/09/2019.
//  Copyright © 2019 ben lönnqvist. All rights reserved.
//

import Foundation

struct Course {
    let category: String
    let desc: String
    let price: String
}
extension Course: CustomStringConvertible {
    var description: String {
        return [category, desc, price].joined(separator: " ").trimmingCharacters(in: .whitespaces).replacingOccurrences(of: "  ", with: " ")
    }
}

struct Lunch {
    let courses: [Course]
    let name: String
}

extension Lunch: CustomStringConvertible {
    var description: String {
        return courses.map{ String(describing: $0) }.joined(separator: "\n")
    }
}

public func explorer() -> String {
    let lang = UserDefaults.standard.string(forKey: "lang") ?? "en"
    let group = DispatchGroup()
    var courses = [Course]()
    var errorMessage = ""
    group.enter()
    DispatchQueue.global().async {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        let date = formatter.string(from: Date())
        let urlString = "https://www.sodexo.fi/ruokalistat/output/daily_json/186/\(date)"
        let url = URL(string: urlString)
        let task = URLSession.shared.dataTask(with: url!)  { (data, response, error) -> Void in
            if error != nil {
                errorMessage = error?.localizedDescription ?? ""
                group.leave()
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String: Any]
                let _jsonCourses = json["courses"] as! [String: [String: String]]
                let jsonCourses = _jsonCourses.sorted( by: { $0.0 < $1.0 })
                for course in jsonCourses {
                    let cat = CFXMLCreateStringByUnescapingEntities(nil, course.value["category"]! as CFString, nil) as String
                    let desc = CFXMLCreateStringByUnescapingEntities(nil, course.value["title_\(lang)"]! as CFString, nil) as String
                    let price = CFXMLCreateStringByUnescapingEntities(nil, course.value["price"]! as CFString, nil) as String
                    courses.append(Course(category: cat+":", desc: desc, price: price))
                }
                group.leave()
            }
            catch {
                errorMessage = error.localizedDescription
                group.leave()
                return
            }
        }
        task.resume()
    }
    group.wait()
    if !errorMessage.isEmpty {
        return errorMessage
    }
    return String(describing: Lunch(courses: courses, name: "explorer"))
}

public func acqua() -> String {
    let lang = UserDefaults.standard.string(forKey: "lang") ?? "en"
    let group = DispatchGroup()
    var courses = [Course]()
    var errorMessage = ""
    group.enter()
    DispatchQueue.global().async {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let date = formatter.string(from: Date())
        let urlString = "https://www.sodexo.fi/ruokalistat/output/daily_json/30/\(date)/\(lang)"
        let url = URL(string: urlString)
        
        let task = URLSession.shared.dataTask(with: url!)  { (data, response, error) -> Void in
            if error != nil {
                errorMessage = error?.localizedDescription ?? ""
                group.leave()
                return
            }
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as! [String: Any]
                let jsonCourses = json["courses"] as! [[String: String]]
                for course in jsonCourses {
                    let cat = CFXMLCreateStringByUnescapingEntities(nil, course["title_\(lang)"]! as CFString, nil) as String
                    let desc = CFXMLCreateStringByUnescapingEntities(nil, course["desc_\(lang)"]! as CFString, nil) as String
                    let price = CFXMLCreateStringByUnescapingEntities(nil, course["price"]! as CFString, nil) as String
                    courses.append(Course(category: cat + (desc.isEmpty ? "": ":"), desc: desc, price: price))
                }
                group.leave()
            }
            catch {
                errorMessage = error.localizedDescription
                group.leave()
                return
            }
        }
        task.resume()
    }
    group.wait()
    if !errorMessage.isEmpty {
        return errorMessage
    }
    return String(describing: Lunch(courses: courses, name: "acqua"))
}

public func himasali() -> String {
    let lang = UserDefaults.standard.string(forKey: "lang") ?? "en"
    return lang
}

