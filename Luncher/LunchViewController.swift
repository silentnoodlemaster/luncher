//
//  LunchViewController.swift
//  Luncher
//
//  Created by ben lönnqvist on 10/09/2019.
//  Copyright © 2019 ben lönnqvist. All rights reserved.
//

import Cocoa

class LunchViewController: NSViewController {
    @IBOutlet weak var textLabel: NSTextField!
    @IBOutlet weak var optButton: NSButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        textLabel.stringValue = ""
    }
    
    func explorerLunch() {
        textLabel.stringValue = explorer()
    }
    func acquaLunch() {
        textLabel.stringValue = acqua()
    }
    func himasaliLunch() {
        textLabel.stringValue = himasali()
    }
    
}
extension LunchViewController {
    // MARK: Storyboard instantiation
    static func freshController() -> LunchViewController {
        //1.
        let storyboard = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
        //2.
        let identifier = NSStoryboard.SceneIdentifier("LunchViewController")
        //3.
        guard let viewcontroller = storyboard.instantiateController(withIdentifier: identifier) as? LunchViewController else {
            fatalError("Why cant i find LunchViewController? - Check Main.storyboard")
        }
        return viewcontroller
    }

}

// MARK: Actions

extension LunchViewController {
    @IBAction func ac(_ sender: NSButton) {
        acquaLunch()
    }

    @IBAction func ex(_ sender: NSButton) {
        explorerLunch()
    }

    @IBAction func hs(_ sender: NSButton) {
        himasaliLunch()
    }
    
    @IBAction func quit(_ sender: NSMenuItem) {
        NSApplication.shared.terminate(sender)
    }
    
    @IBAction func lang(_ sender: NSMenuItem) {
        if sender.title == "Suomi" {
            UserDefaults.standard.set("fi", forKey: "lang")
        } else if sender.title == "English" {
            UserDefaults.standard.set("en", forKey: "lang")
        }
    }
    
    @IBAction func showOptionsMenu(_ sender: NSButton) {
        let optMenu = NSMenu(title: "Options Menu")
        let quitItem = NSMenuItem(title: "Quit Luncher", action:#selector(self.quit), keyEquivalent: "")
        let fiItem = NSMenuItem(title: "Suomi", action:#selector(self.lang), keyEquivalent: "")
        let enItem = NSMenuItem(title: "English", action:#selector(self.lang), keyEquivalent: "")
        if UserDefaults.standard.string(forKey: "lang") == "fi" {
            fiItem.state = NSControl.StateValue.on
        } else {
            enItem.state = NSControl.StateValue.on
        }
        optMenu.addItem(fiItem)
        optMenu.addItem(enItem)
        optMenu.addItem(NSMenuItem.separator())
        optMenu.addItem(quitItem)
        optMenu.popUp(positioning: nil, at: NSPoint(x: 0, y: optButton.bounds.height), in: optButton)
    }
}
